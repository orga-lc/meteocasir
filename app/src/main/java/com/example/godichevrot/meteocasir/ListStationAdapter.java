package com.example.godichevrot.meteocasir;

import android.app.Application;
import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by chevrotl on 21/01/2016.
 */
public class ListStationAdapter implements ListAdapter {

    Map<String,String> idAndLibelle ;
    Context context ;

    public ListStationAdapter(Context context){
        this.idAndLibelle = ListeStation.idAndLibelle;
        this.context = context ;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return this.idAndLibelle.size();
    }

    @Override
    public Object getItem(int position) {
        String[] mapKeys = new String[this.idAndLibelle.size()];
        int pos = 0;
        for (String key : this.idAndLibelle.keySet()) {
            mapKeys[pos++] = key;
        }
        return mapKeys[position];
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(this.idAndLibelle.get(position));
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if(view  == null) {
            view = LayoutInflater.from(this.context).inflate(R.layout.item_liste_station, parent, false);
        }
        String id = (String) getItem(position);
        String libelle = idAndLibelle.get(id);

        TextView tvLib = (TextView) view.findViewById(R.id.tv_libStation);
        Button btnLib = (Button) view.findViewById(R.id.btn_fav);

        tvLib.setText(libelle);
        tvLib.setTag(id);
        btnLib.setTag(id);

        //TODO Rentrer info dans la vue
        return view;
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
