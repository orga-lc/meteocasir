package com.example.godichevrot.meteocasir;

/**
 * Created by chevrotl on 21/01/2016.
 */
public class Station {
    private String id ;
    private String libelle ;
    private String latitude ;
    private String longitude;
    private String altitude;

    public Station(String id, String libelle, String latitude, String longitude, String altitude) {
        this.id = id;
        this.libelle = libelle;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAltitude() {
        return altitude;
    }

    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }
}
