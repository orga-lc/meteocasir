package com.example.godichevrot.meteocasir;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * Created by chevrotl on 11/02/2016.
 */
public class AsyncTaskReleveStation extends AsyncTask<Object, String, String> {
    String idStationToLoad ;
    ListView listView ;
    Context context ;
    @Override
    protected String doInBackground(Object... params) {
    //    act = (ListeStationActivity)params[0];
      //  intent = (Intent)params[1];
        idStationToLoad = (String)params[0];
        listView = (ListView) params[1];
        context = (Context) params[2];




        try {
            // URL url = new URL("http://gigondas/~chevrotl/tp_meteo/index.php?"+lien);
            URL url = new URL("http://intranet.iut-valence.fr/~chevrotl/tp_meteo/index.php?idReleveStation="+idStationToLoad);

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            String authorization = Constantes.LOGIN_HTTPREQUEST + ":" + Constantes.PASSWORD_HTTPREQUEST;

            Authenticator.setDefault(new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(Constantes.LOGIN_HTTPREQUEST, Constantes.PASSWORD_HTTPREQUEST.toCharArray());
                }
            });

            String response = "{\"array\":";
            String ligne = "";
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader r = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));

            while ((ligne = r.readLine()) != null)
                response += ligne;
            response+="}";

            ListeStation.listeReleves = getReleveFromJson(response);
            urlConnection.disconnect();

            Log.w("ZOB", response);
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        listView.setAdapter(new AdapterListViewMain(context));
    }

    private ArrayList<Releve> getReleveFromJson(String json){
        ArrayList<Releve> listReleve = new ArrayList<Releve>();
        try {
            JSONObject jObject = new JSONObject(json);
            JSONArray array = jObject.getJSONArray("array");
            for (int i = 0; i < array.length(); i++) {
                try {
                    JSONObject infoStation = array.getJSONObject(i);
                    String id = infoStation.getString("station");
                    String quand = infoStation.getString("quand");
                    String temp1 = infoStation.getString("temp1");
                    String temp2 = infoStation.getString("temp2");
                    String pressure = infoStation.getString("pressure");
                    String lux = infoStation.getString("lux");
                    String hygro = infoStation.getString("hygro");
                    String windSpeed = infoStation.getString("windSpeed");
                    String windDir = infoStation.getString("windDir");

                    Releve r = new Releve(id,quand,temp1,temp2,pressure,lux,hygro,windSpeed,windDir);
                    listReleve.add(r);
                    //idAndLibelle.put(id, libelle);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listReleve;
    }
}
