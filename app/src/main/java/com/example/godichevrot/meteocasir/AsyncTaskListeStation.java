package com.example.godichevrot.meteocasir;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by chevrotl on 17/12/2015.
 */
public class AsyncTaskListeStation extends AsyncTask<Object, String, String> {
    Intent intent ;
    MainActivity mainActivity ;
    @Override
    protected String doInBackground(Object... params) {
        String lien = "";
        String s = (String)params[0];
        intent = (Intent)params[1];
        mainActivity = (MainActivity) params[2];

        try {
            // URL url = new URL("http://gigondas/~chevrotl/tp_meteo/index.php?"+lien);
            URL url = new URL("http://intranet.iut-valence.fr/~chevrotl/tp_meteo/index.php?listeStation");

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            String authorization = Constantes.LOGIN_HTTPREQUEST + ":" + Constantes.PASSWORD_HTTPREQUEST;

            Authenticator.setDefault(new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(Constantes.LOGIN_HTTPREQUEST, Constantes.PASSWORD_HTTPREQUEST.toCharArray());
                }
            });

            String response = "{\"array\":";
            String ligne = "";
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader r = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));

            while ((ligne = r.readLine()) != null)
                response += ligne;

            response+="}";
            urlConnection.disconnect();
            ListeStation.idAndLibelle = getListeStationFromJson(response);
            mainActivity.startActivity(intent);

            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

    }

    private TreeMap<String,String> getListeStationFromJson(String json) {
        TreeMap<String, String> idAndLibelle = new TreeMap<String, String>();
        try {
            JSONObject jObject = new JSONObject(json);
            JSONArray array = jObject.getJSONArray("array");
            for (int i = 0; i < array.length(); i++) {
                try {
                    JSONObject infoStation = array.getJSONObject(i);
                    String id = infoStation.getString("id");
                    String libelle = infoStation.getString("libellé");
                    idAndLibelle.put(id, libelle);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return idAndLibelle;
    }


}
