package com.example.godichevrot.meteocasir;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class DetailReleveActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_releve);
        Intent intent = this.getIntent();
        TextView tvId = (TextView) findViewById(R.id.tvLibelleStationReleve);
        tvId.setText(intent.getStringExtra("id"));

        TextView tvQuand = (TextView) findViewById(R.id.tvLibelleDateReleve);
        tvQuand.setText(intent.getStringExtra("quand"));

        TextView tvTemp1 = (TextView) findViewById(R.id.tvLibelleTemp1Releve);
        tvTemp1.setText(intent.getStringExtra("temp1"));

        TextView tvTemp2 = (TextView) findViewById(R.id.tvLibelleTemp2Releve);
        tvTemp2.setText(intent.getStringExtra("temp2"));

        TextView tvPressure = (TextView) findViewById(R.id.tvLibellePressureReleve);
        tvPressure.setText(intent.getStringExtra("pressure"));

        TextView tvLux = (TextView) findViewById(R.id.tvLibelleLuxReleve);
        tvLux.setText(intent.getStringExtra("lux"));

        TextView tvHygro = (TextView) findViewById(R.id.tvLibelleHygroReleve);
        tvHygro.setText(intent.getStringExtra("hygro"));

        TextView tvWindSpeed = (TextView) findViewById(R.id.tvLibelleWindSpeedReleve);
        tvWindSpeed.setText(intent.getStringExtra("windSpeed"));

        TextView tvWindDir = (TextView) findViewById(R.id.tvLibelleWindDirReleve);
        tvWindDir.setText(intent.getStringExtra("windDir"));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail_releve, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
