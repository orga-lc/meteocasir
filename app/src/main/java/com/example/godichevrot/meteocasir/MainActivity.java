package com.example.godichevrot.meteocasir;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


public class MainActivity extends ActionBarActivity {

    SharedPreferences prefs ;
    Map<String,String> listeIdLibelleStation ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.prefs = getSharedPreferences(Constantes.PREFS_NAME,0);
        setContentView(R.layout.layout_principal);
        ListView lv = (ListView) findViewById(R.id.listViewReleve);
        Intent intent = this.getIntent();

        String idIntent = intent.getStringExtra("id");
        if(idIntent != null){
            Station s = ListeStation.getStationById(idIntent);
            setMainStationDisplay(s.getId(), s.getLibelle(), s.getLatitude(), s.getLongitude(), s.getAltitude());
            new AsyncTaskReleveStation().execute(idIntent, lv, this);
        }
        else {

            ListeStation.listeStation = new ArrayList<Station>();
            ListeStation.listeReleves = new ArrayList<Releve>();

            ListeStation.listeStation.add(new Station("Alboussière", "Station d'Albousière", "50", "75", "100"));

            String id = prefs.getString(Constantes.PREFS_ID_STATION, "Alboussière");
            new AsyncTaskReleveStation().execute(id, lv, this);
            setMainStationDisplayFromPref();
        }



    }

    @Override
    protected void onResume() {
        super.onResume();
        ListView lv = (ListView) findViewById(R.id.listViewReleve);
        Intent intent = this.getIntent();
        String idIntent = intent.getStringExtra("id");

        if(idIntent == null){
            String id = prefs.getString(Constantes.PREFS_ID_STATION, "DEFAULT");
            new AsyncTaskReleveStation().execute(id, lv, this);
            new AsyncTaskInfoStation().execute(null, null, id, this);
        }
        else {
            new AsyncTaskReleveStation().execute(idIntent, lv, this);
            intent.removeExtra("id");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setMainStationDisplay(String id, String libelle, String latitude, String longitude, String altitude){
        TextView tvId = (TextView) findViewById(R.id.textViewTitreIdStation);
        TextView tvLib = (TextView) findViewById(R.id.tvLibelleStation);
        TextView tvLat = (TextView) findViewById(R.id.tvLatitudeStation);
        TextView tvLong = (TextView) findViewById(R.id.tvLongitudeStation);
        TextView tvAlt = (TextView) findViewById(R.id.tvAltitudeStation);

        tvId.setText(id);
        tvLib.setText(libelle);
        tvLat.setText(latitude);
        tvLong.setText(longitude);
        tvAlt.setText(altitude);
    }

    public void setMainStationDisplayFromPref(){
        TextView tvId = (TextView) findViewById(R.id.textViewTitreIdStation);
        TextView tvLib = (TextView) findViewById(R.id.tvLibelleStation);
        TextView tvLat = (TextView) findViewById(R.id.tvLatitudeStation);
        TextView tvLong = (TextView) findViewById(R.id.tvLongitudeStation);
        TextView tvAlt = (TextView) findViewById(R.id.tvAltitudeStation);

        String idStation = prefs.getString(Constantes.PREFS_ID_STATION, "DEFAULT") ;
        Station station = ListeStation.getStationById(idStation);

        if(station != null){
            tvId.setText(station.getId());
            tvLib.setText(station.getLibelle());
            tvLat.setText(station.getLatitude());
            tvLong.setText(station.getLongitude());
            tvAlt.setText(station.getAltitude());
        }


    }

    public void affichageListeStation(View v){
        Intent intent = new Intent(this, ListeStationActivity.class);
        new AsyncTaskListeStation().execute("",intent,this);


    }


    BufferedReader in = null;
    String data = null;


}
