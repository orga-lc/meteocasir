package com.example.godichevrot.meteocasir;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by chevrotl on 28/01/2016.
 */
public class ListeStation {

    public static ArrayList<Station> listeStation;
    public static TreeMap<String,String> idAndLibelle ;
    public static ArrayList<Releve> listeReleves;

    public static Station getStationById(String id){
        for(Station s : listeStation){
            if(s.getId().equals(id)){
                return s ;
            }
        }
    return null ;
    }

}
