package com.example.godichevrot.meteocasir;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class ListeStationActivity extends ActionBarActivity {

    ArrayList listStation ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_station);

        ListView lv = (ListView) findViewById(R.id.listViewStation);
        lv.setAdapter(new ListStationAdapter(this));

    }

    public void setStationFavorite(View v){
        String idStation = (String) v.getTag() ;
        Toast.makeText(this, "Choix sauvegarde", Toast.LENGTH_SHORT).show();
        SharedPreferences.Editor editor = getSharedPreferences(Constantes.PREFS_NAME,0).edit();
        editor.putString(Constantes.PREFS_ID_STATION, idStation) ;
        editor.apply();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_liste_station, menu);
        return true;
    }

    public void getStation(View v){
        String id = (String) v.getTag();
        Intent myIntent = new Intent(this, MainActivity.class);
        myIntent.putExtra("id",id);
        new AsyncTaskInfoStation().execute(this, myIntent, id, null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
