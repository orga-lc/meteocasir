package com.example.godichevrot.meteocasir;

/**
 * Created by chevrotl on 11/02/2016.
 */
public class Releve {
    private String idStation;
    private String quand ;
    private String temp1 ;
    private String temp2 ;
    private String pressure ;
    private String lux ;
    private String hygro ;
    private String windSpeed ;
    private String windDir ;

    public Releve(String idStaion, String quand, String temp1, String temp2, String pressure, String lux, String hygro, String windSpeed, String windDir) {
        this.idStation = idStaion;
        this.quand = quand;
        this.temp1 = temp1;
        this.temp2 = temp2;
        this.pressure = pressure;
        this.lux = lux;
        this.hygro = hygro;
        this.windSpeed = windSpeed;
        this.windDir = windDir;
    }

    public String getIdStation() {
        if(idStation == null){return "";}
        return idStation;
    }

    public void setIdStation(String idStation) {
        this.idStation = idStation;
    }

    public String getQuand() {
        if(quand == null){return "";}
        return quand;
    }

    public void setQuand(String quand) {
        this.quand = quand;
    }

    public String getTemp1() {
        if(temp1 == null){return "";}
        return temp1;
    }

    public void setTemp1(String temp1) {
        this.temp1 = temp1;
    }

    public String getTemp2() {
        if(temp2 == null){return "";}
        return temp2;
    }

    public void setTemp2(String temp2) {
        this.temp2 = temp2;
    }

    public String getPressure() {
        if(pressure == null){return "";}
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getLux() {
        if(lux == null){return "";}
        return lux;
    }

    public void setLux(String lux) {
        this.lux = lux;
    }

    public String getHygro() {
        if(hygro == null){return "";}
        return hygro;
    }

    public void setHygro(String hygro) {
        this.hygro = hygro;
    }

    public String getWindSpeed() {
        if(windSpeed == null){return "";}
        return windSpeed;
    }

    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getWindDir() {
        if(windDir == null){return "";}
        return windDir;
    }

    public void setWindDir(String windDir) {
        this.windDir = windDir;
    }
}
