package com.example.godichevrot.meteocasir;

import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

/**
 * Created by chevrotl on 11/02/2016.
 */
public class AdapterListViewMain implements ListAdapter {

    Context context ;
    public AdapterListViewMain(Context context) {
        this.context = context;
    }



    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return ListeStation.listeReleves.size();
    }

    @Override
    public Object getItem(int position) {
        return ListeStation.listeReleves.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if(view  == null) {
            view = LayoutInflater.from(this.context).inflate(R.layout.item_liste_releve, parent, false);
        }
        TextView tv = (TextView) view.findViewById(R.id.dateReleveItem);
        tv.setText(ListeStation.listeReleves.get(position).getQuand());
        final Intent intent = new Intent(this.context, DetailReleveActivity.class);

        intent.putExtra("id", ListeStation.listeReleves.get(position).getIdStation());
        intent.putExtra("quand", ListeStation.listeReleves.get(position).getQuand());
        intent.putExtra("temp1", ListeStation.listeReleves.get(position).getTemp1());
        intent.putExtra("temp2", ListeStation.listeReleves.get(position).getTemp2());
        intent.putExtra("pressure", ListeStation.listeReleves.get(position).getPressure());
        intent.putExtra("lux", ListeStation.listeReleves.get(position).getLux());
        intent.putExtra("hygro", ListeStation.listeReleves.get(position).getHygro());
        intent.putExtra("windSpeed", ListeStation.listeReleves.get(position).getWindSpeed());
        intent.putExtra("windDir", ListeStation.listeReleves.get(position).getWindDir());

        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(intent);
            }
        });



        return view ;
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
