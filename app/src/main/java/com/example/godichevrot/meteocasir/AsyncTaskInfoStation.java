package com.example.godichevrot.meteocasir;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * Created by Lucas on 10/02/2016.
 */
public class AsyncTaskInfoStation extends AsyncTask<Object, String, String> {

    ListeStationActivity act;
    Intent intent ;
    String idStationToLoad ;
    MainActivity mainActivity ;




    @Override
    protected String doInBackground(Object... params) {

        act = (ListeStationActivity)params[0];
        intent = (Intent)params[1];
        idStationToLoad = (String)params[2];
        mainActivity = (MainActivity)params[3];



        try {
            // URL url = new URL("http://gigondas/~chevrotl/tp_meteo/index.php?"+lien);
            URL url = new URL("http://intranet.iut-valence.fr/~chevrotl/tp_meteo/index.php?idInfoStation="+idStationToLoad);

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            String authorization = Constantes.LOGIN_HTTPREQUEST + ":" + Constantes.PASSWORD_HTTPREQUEST;

            Authenticator.setDefault(new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(Constantes.LOGIN_HTTPREQUEST, Constantes.PASSWORD_HTTPREQUEST.toCharArray());
                }
            });

            String response = "";
            String ligne = "";
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader r = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));

            while ((ligne = r.readLine()) != null)
                response += ligne;

            getInfosStationFromJson(response);
            urlConnection.disconnect();

            Log.w("ZOB", response);
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        if(mainActivity != null){
            mainActivity.setMainStationDisplayFromPref();
        }
        else {
            act.startActivity(intent);
        }

    }

    private void getInfosStationFromJson(String json){
        try {
            JSONObject jObject = new JSONObject(json);
            String id = jObject.getString("id");
            String libelle = jObject.getString("libellé");
            String latitude = jObject.getString("latitude");
            String longitude = jObject.getString("longitude");
            String altitude = jObject.getString("altitude");

            Station s = new Station(id,libelle,latitude,longitude,altitude);

            if(ListeStation.getStationById(id) == null)
                ListeStation.listeStation.add(s);



        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
